#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging
import re
import webapp2
from google.appengine.api import mail
from webapp2_extras.auth import InvalidAuthIdError, InvalidPasswordError
from handlers.base_handler import BaseHandler, user_required, user_authorized
from handlers.bills_handler import BillHandler, BillsHandler


USER_RE = re.compile(r'^[a-zA-Z0-9_-]{3,20}$')
NAME_RE = re.compile(r'^[a-zA-Z]{2,20}$')
PASSWORD_RE = re.compile(r'^.{3,20}$')
EMAIL_RE = re.compile(
    r'^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$')


def valid_email(email):
    return EMAIL_RE.match(email)


def valid_username(username):
    return USER_RE.match(username)


def valid_name(name):
    return NAME_RE.match(name)


def valid_password(password):
    return PASSWORD_RE.match(password)


class ForgotPasswordHandler(BaseHandler):
    def _serve_page(self, not_found=False):
        username = self.request.get('username')
        params = {
            'username': username,
            'not_found': not_found
        }
        self.render_template('forgot.html', params)

    def get(self):
        self._serve_page()

    def post(self):
        username = self.request.get('username')

        user = self.user_model.get_by_auth_id(username)
        if not user:
            logging.info('Could not find any user entry for username %s',
                         username)
            self._serve_page(not_found=True)
            return

        user_id = user.get_id()
        token = self.user_model.create_signup_token(user_id)

        verification_url = self.uri_for('verification', type='p',
                                        user_id=user_id, signup_token=token,
                                        _full=True)

        msg = 'Send an email to user in order to reset their password. \
        They will be able to do so by visiting <a href="{url}">{url}</a>'

        self.display_message(msg.format(url=verification_url))


class MainHandler(BaseHandler):
    def get(self):
        self.render_template('home.html')


class BillsHomeHandler(BaseHandler):
    @user_required
    def get(self):
        self.render_template('bills.html')


class LoginHandler(BaseHandler):
    def _serve_page(self, failed=False):
        username = self.request.get('username')
        params = {
            'username': username,
            'failed': failed
        }
        self.render_template('login.html', params)

    def get(self):
        self._serve_page()

    def post(self):
        username = self.request.get('username')
        password = self.request.get('password')
        try:
            user = self.auth.get_user_by_password(username, password,
                                                  remember=True,
                                                  save_session=True)
            self.redirect(self.uri_for('home'))
        except (InvalidAuthIdError, InvalidPasswordError) as e:
            # logging.info('Login failed for user %s because of %s', username, type(e))
            self._serve_page(True)


class LogoutHandler(BaseHandler):
    def get(self):
        self.auth.unset_session()
        self.redirect(self.uri_for('home'))


class SignupHandler(BaseHandler):
    def render_form(self, username='', email='', name='',
                    lastname='', errors=''):
        params = {
            'username': username,
            'email': email,
            'name': name,
            'lastname': lastname,
            'errors': errors
        }
        self.render_template('signup.html', params)

    def get(self):
        self.render_form()

    def post(self):
        have_error = False
        username = self.request.get('username')
        email = self.request.get('email')
        name = self.request.get('name')
        password = self.request.get('password')
        verify = self.request.get('verify')
        lastname = self.request.get('lastname')

        params = dict(username=username, email=email, name=name,
                      lastname=lastname)

        if not valid_username(username):
            params['error_username'] = "That's not a valid username."
            have_error = True

        if not valid_password(password):
            params['error_password'] = "That wasn't a valid password."
            have_error = True
        elif password != verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = True

        if not valid_email(email):
            params['error_email'] = "That's not a valid email."
            have_error = True

        if not valid_name(name):
            params['error_name'] = "Please enter a valid first name."
            have_error = True

        if not valid_name(lastname):
            params['error_lastname'] = "Please enter a valid last name."
            have_error = True

        if have_error:
            self.render_template('signup.html', params)
        else:
            unique_properties = ['email_address']
            user_data = self.user_model.create_user(username, unique_properties,
                                                    email_address=email,
                                                    name=name,
                                                    password_raw=password,
                                                    last_name=lastname,
                                                    verified=False,
                                                    sec_groups=[username,
                                                                'user'])
            if not user_data[0]:
                self.display_message(
                    'Unable to create user for email %s because of duplicate keys %s' % (
                        username, user_data[1]))
                return

            user = user_data[1]
            user_id = user.get_id()

            token = self.user_model.create_signup_token(user_id)

            verification_url = self.uri_for('verification', type='v',
                                            user_id=user_id, signup_token=token,
                                            _full=True)

            msg = 'Thanks for signing up with Budgit Money. \
            Please verify your email address by clicking <a href="{url}">here</a>'

            sender_address = "Jason Dearth <jasondearth@gmail.com>"
            subject = "Confirm your registration"
            body = msg.format(url=verification_url)
            mail.send_mail(sender_address, email, subject, body)
            self.display_message(msg.format(url=verification_url))


class SetPasswordHandler(BaseHandler):
    @user_required
    def post(self):
        password = self.request.get('password')
        old_token = self.request.get('t')

        if not password or password != self.request.get('confirm_password'):
            self.display_message('passwords do not match')
            return

        user = self.user
        user.set_password(password)
        user.put()

        # remove signup token, we don't want users to come back with an old link
        self.user_model.delete_signup_token(user.get_id(), old_token)

        self.display_message('Password updated')


class VerificationHandler(BaseHandler):
    def get(self, *args, **kwargs):
        user = None
        user_id = kwargs['user_id']
        signup_token = kwargs['signup_token']
        verification_type = kwargs['type']

        user, ts = self.user_model.get_by_auth_token(int(user_id),
                                                     signup_token,
                                                     'signup')

        if not user:
            logging.info(
                'Could not find any user with id "%s" signup token "%s"',
                user_id, signup_token)
            self.abort(404)

        # store user data in the session
        self.auth.set_session(self.auth.store.user_to_dict(user), remember=True)

        if verification_type == 'v':
            # remove signup token, we don't want users to come back with an old link
            self.user_model.delete_signup_token(user.get_id(), signup_token)

            if not user.verified:
                user.verified = True
                user.put()

            self.display_message('User email address has been verified.')
            return

        elif verification_type == 'p':
            # supply user to the page
            params = {
                'user': user,
                'token': signup_token
            }
            self.render_template('resetpassword.html', params)
        else:
            logging.info('verification type no supported')
            self.abort(404)


config = {
    'webapp2_extras.auth': {
        'user_model': 'models.security.User',
        'user_attributes': ['name']
    },
    'webapp2_extras.sessions': {
        'secret_key': 'YOUR_SECRET_KEY'
    }
}

app = webapp2.WSGIApplication([
                                  webapp2.Route('/', MainHandler, name='home'),
                                  webapp2.Route('/api/bill/<bill_id:\d+>',
                                                BillHandler, name='bill'),
                                  webapp2.Route('/api/bills', BillsHandler,
                                                name='bills'),
                                  webapp2.Route('/bills', BillsHomeHandler,
                                                name='bills_home'),
                                  webapp2.Route('/login', LoginHandler,
                                                name='login'),
                                  webapp2.Route('/logout', LogoutHandler,
                                                name='logout'),
                                  webapp2.Route('/signup', SignupHandler,
                                                name='signup'),
                                  webapp2.Route('/forgot',
                                                ForgotPasswordHandler,
                                                name='forgot'),
                                  webapp2.Route('/password',
                                                SetPasswordHandler),
                                  webapp2.Route(
                                      '/<type:v|p>/<user_id:\d+>-<signup_token:.+>',
                                      VerificationHandler,
                                      name='verification')
                              ], debug=True, config=config)
