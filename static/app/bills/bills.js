angular.module('billApp', ['ngRoute', 'billApp.services', 'billApp.controllers', 'myDatePicker']).
    config(function ($routeProvider, $httpProvider) {
        $routeProvider.when('/bill-add', {templateUrl: '/static/app/bills/bill-add.html', controller: 'BillAddCtrl'});
        $routeProvider.when('/bills', {templateUrl: '/static/app/bills/bills.html', controller: 'BillListCtrl'});
        $routeProvider.when('/bills-register', {templateUrl: '/static/app/bills/bills-register.html', controller: 'BudgetViewCtrl'});
        $routeProvider.otherwise({redirectTo: '/bills'});
    });