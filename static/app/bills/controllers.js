var app = angular.module('billApp.controllers', ['ui.bootstrap']);

var frequencies = [
        {'value': 'once', 'text': 'Once'},
        {'value': 'daily', 'text': 'Daily'},
        {'value': 'weekly', 'text': 'Weekly'},
        {'value': 'monthly', 'text': 'Monthly'}];

app.controller('BillAddCtrl', ['$scope', 'billsFactory', '$location',
    function($scope, billsFactory, $location) {
        $scope.frequencies = frequencies;
        $scope.bill = {frequency : $scope.frequencies[0].value};
        $scope.addBill = function() {
            billsFactory.create($scope.bill).then(function(data) {
                $location.path('/bills');
            });
        };
    }]).controller('BillListCtrl', ['$scope', 'billFactory', 'billsFactory', '$location',
    function($scope, billFactory, billsFactory, $location) {
        $scope.frequencies = frequencies;
        $scope.deleteBill = function (delBill) {
            var idx = $scope.bills.indexOf(delBill);
            billFactory.delete(delBill.id).then(function() {
                $scope.bills.splice(idx, 1);
            });
        };

        $scope.editBill = function (bill) {
            bill.edit = true;
        };

        $scope.saveBill = function (bill) {
            var idx = $scope.bills.indexOf(bill);
            billFactory.put(bill.id, angular.toJson(bill)).then(function() {
                bill.edit = false;
            });
        };

        billsFactory.query().success(function(bills) {
            Array.prototype.forEach.call(bills, function(bill) {
                bill.nextDueDate = new Date(Date.parse(bill.nextDueDate));
                bill.nextDueDate.setMinutes(bill.nextDueDate.getMinutes() +
                    bill.nextDueDate.getTimezoneOffset());
                bill.lastDueDate = new Date(Date.parse(bill.lastDueDate));
                bill.lastDueDate.setMinutes(bill.lastDueDate.getMinutes() +
                    bill.lastDueDate.getTimezoneOffset());
            });
            $scope.bills = bills;

        });
    }]).controller('BudgetViewCtrl', ['$scope', 'billsFactory', '$location',
    function($scope, billsFactory, $location) {
        sortByKey = function (array, key) {
            return array.sort(function(a, b) {
                var x = a[key];
                var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        };

        $scope.calculateRegister = function () {
            var lastBalance = $scope.registry.startingBalance;
            if (angular.isNumber(lastBalance)) {
                Array.prototype.forEach.call($scope.budget, function (line) {
                    if (line.credit == true) {
                        line.balance = lastBalance + line.amount;
                    } else {
                        line.balance = lastBalance - line.amount;
                    }
                    lastBalance = line.balance;
                });
                $scope.showRegistry = true;
            }
        };

        billsFactory.query().success(function(bills) {
            var budget = [];
            $scope.registry = new Object();
            Array.prototype.forEach.call(bills, function(bill) {
                var billDate = new Date(Date.parse(bill.nextDueDate));
                var lastBillDate = new Date(Date.parse(bill.lastDueDate));
                var addDays = 1;
                if (bill.frequency.toString() == "monthly") {
                    addDays = 30;
                } else if (bill.frequency.toString() == "weekly") {
                    addDays = 7;
                }
                do {
                    var thisBill = new Object();
                    thisBill.description = bill.description;
                    thisBill.nextDueDate = new Date(billDate.getTime());
                    thisBill.amount = bill.amount;
                    thisBill.credit = bill.credit;
                    budget.push(thisBill);
                    billDate.setDate(billDate.getDate() + addDays);
                } while (bill.frequency != 'once' && billDate <= lastBillDate);
            });
            $scope.budget = sortByKey(budget, 'nextDueDate');
            $scope.calculateRegister();
        });
    }]).directive('ngBlur', ['$parse', function($parse) {
    return function(scope, element, attr) {
        var fn = $parse(attr['ngBlur']);
        element.bind('blur', function(event) {
            scope.$apply(function() {
                fn(scope, {$event:event});
            });
        });
    }
}]);