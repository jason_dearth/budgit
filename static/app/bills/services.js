var services = angular.module('billApp.services', []);

services.factory('billsFactory', function ($http) {
    var url = '/api/bills';
    return {
        query: function() {
            return $http.get(url);
        },
        create: function(data) {
            return $http.post(url, data);
        }
    };
});

services.factory('billFactory', function ($http) {
    var url = '/api/bill/';
    return {
        delete: function(billId) {
            return $http.delete(url + billId);
        },
        put: function(billId, data) {
            return $http.put(url + billId, data);
        }
    };
});