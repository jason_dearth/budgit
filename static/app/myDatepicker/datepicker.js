'use strict';

angular
    .module('myDatePicker', [
        'ui.router',
        'ui.bootstrap'
    ])
    .directive('datePick', function () {
        var mprefix = 'jld';
        var linkFunc = function ($scope) {
            $scope.mprefix = mprefix;
            $scope.open = function (event) {
                event.preventDefault();
                event.stopPropagation();
                $scope.opened = true;
            };
            return open;
        };
        return {
            restrict: 'E',
            replace: true,
            scope: {
                ngModel: '=',
                dateOptions: '=',
                opened: '='
            },
            link: linkFunc,
            templateUrl: 'static/app/myDatepicker/datepicker.html'
        };
    })
    .controller('dateCtrl', function ($scope) {
        $scope.opened = false;
        $scope.dateOptions = {
            'year-format': 'yy',
            'show-weeks': false
        };
    });