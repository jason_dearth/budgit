__author__ = 'jasondearth'

from google.appengine.ext import ndb


def dump_datetime(value):
    """Deserialize datetime objects into string form for JSON processing."""
    if value is None:
        return None
    date_string = value.strftime('%Y-%m-%dT%H:%M:%SZ')
    return date_string


class Bill(ndb.Model):
    description = ndb.StringProperty(required=True)
    user = ndb.KeyProperty(kind='User')
    next_due_date = ndb.DateProperty()
    status = ndb.StringProperty(choices={'due', 'late', 'submitted', 'cleared'})
    last_due_date = ndb.DateProperty()
    amount = ndb.FloatProperty(default=105.6)
    credit = ndb.BooleanProperty(default=False)
    frequency_factor = ndb.IntegerProperty()
    frequency = ndb.StringProperty(choices={'once', 'daily', 'weekly', 'monthly'})

    def as_dict(self):
        return {'id': self.key.id(),
                'description': self.description,
                'nextDueDate': str(dump_datetime(self.next_due_date)),
                'status': self.status,
                'lastDueDate': str(dump_datetime(self.last_due_date)),
                'amount': self.amount,
                'credit': self.credit,
                'frequency': self.frequency,
                'frequencyFactor': self.frequency_factor}

    @classmethod
    def add_user_bill(cls, description, user_key, next_due_date, last_due_date,
                      amount, parent='', status='due', credit=False,
                      frequency='monthly', frequency_factor=1):
        if frequency == 'once':
            bill = Bill(description=description, user=user_key,
                        next_due_date=next_due_date, amount=amount,
                        status=status, credit=credit, parent=parent,
                        frequency=frequency)
        else:
            bill = Bill(description=description, user=user_key,
                        next_due_date=next_due_date,
                        last_due_date=last_due_date, amount=amount,
                        status=status, credit=credit, parent=parent,
                        frequency=frequency, frequency_factor=frequency_factor)
        bill.put()
        return bill.key

    @classmethod
    def get_user_bills(cls, user_key, parent=''):
        try:
            return cls.query(cls.user == user_key, ancestor=parent)
        except:
            return None
