__author__ = 'jasondearth'

import re
import unittest
from test_base import BaseTests


class HomePageTests(BaseTests):
    def test_main_handler(self):
        response = self.testapp.get('/')
        self.assertIn('Budgit Money', response.body)
        self.assertEqual(response.status, '200 OK')


class SignupPageTests(BaseTests):
    def test_get_signup_form(self):
        response = self.testapp.get('/signup')
        self.assertIn('Register a new user', response.body)
        self.assertIn('action="/signup"', response.body)

    def test_post_blank_form(self):
        response = self.testapp.post('/signup')
        self.assertIn("That&#39;s not a valid username.", response.body)
        self.assertIn("That wasn&#39;t a valid password", response.body)
        self.assertIn("That&#39;s not a valid email", response.body)
        self.assertIn("Please enter a valid first name", response.body)
        self.assertIn("Please enter a valid last name", response.body)

    def test_post_username_passes_server_validation(self):
        response = self.testapp.post('/signup',
                                     params={'username': 'jasondearth'})
        self.assertNotIn("That&#39;s not a valid username", response.body)

    def test_post_email_passes_server_validation(self):
        response = self.testapp.post('/signup',
                                     params={'email': 'jasondearth@gmail.com'})
        self.assertNotIn("That&#39;s not a valid email", response.body)

    def test_post_email_invalid_server_validation(self):
        response = self.testapp.post('/signup', params={'email': 'jasondearth'})
        self.assertIn("That&#39;s not a valid email", response.body)

    def test_post_signup_form_with_password_mismatch(self):
        response = self.testapp.post('/signup',
                                     params={'username': 'jasondearth',
                                             'email': 'jasondearth@gmail.com',
                                             'password': 'right',
                                             'verify': 'wrong'})
        self.assertIn("Your passwords didn&#39;t match", response.body)

    def test_post_success_user_signup(self):
        response = self.testapp.post('/signup',
                                     params={'username': 'jasondearth',
                                             'email': 'jasondearth@gmail.com',
                                             'password': 'password',
                                             'verify': 'password',
                                             'name': 'Jason',
                                             'lastname': 'Dearth'})
        self.assertIn('Thanks for signing up with Budgit Money', response.body)
        LINK_RE = re.compile(r'/v/\d+-[^"]+')
        link = LINK_RE.search(response.body).group()
        verified = self.testapp.get(link)
        self.assertIn('User email address has been verified', verified.body)
        auth_test = self.testapp.get('/bills')
        self.assertIn('ng-app="billApp"', auth_test.body)

    def test_get_login_page(self):
        response = self.testapp.get('/login')
        self.assertIn('Login:', response.body)

    def test_user_login_success(self):
        username = 'jdearth'
        password = 'jdearth'
        self.add_user(username=username, password_raw=password, verified=True)
        response = self.testapp.post('/login', params={'username': username,
                                                       'password': password})
        self.assertEqual(response.status_int, 302)
        # TODO: change the below test before testing on app engine
        self.assertEqual(response.location, 'http://localhost/')

    def test_user_login_failure_wrong_password(self):
        username = 'jdearth'
        password = 'jdearth'
        self.add_user(username=username, password_raw=password, verified=True)
        response = self.testapp.post('/login',
                                     params={'username': username,
                                             'password': 'wrong'})
        self.assertIn('Login failed!', response.body)
        self.assertIn('Check your credentials and try again.', response.body)

    def test_user_login_failure_username_not_exists(self):
        response = self.testapp.post('/login',
                                     params={'username': 'made_up',
                                             'password': 'password'})
        self.assertIn('Login failed!', response.body)
        self.assertIn('Check your credentials and try again.', response.body)

    def test_user_logout(self):
        username = 'jdearth'
        password = 'password'
        self.add_user(username=username, password_raw=password, verified=True)
        self.testapp.post('/login',
                          params={'username': username, 'password': password})
        self.assertEqual(len(self.testapp.cookies), 1)
        self.testapp.get('/logout')
        self.assertEqual(len(self.testapp.cookies), 1)
        response = self.testapp.get('/bills')
        self.assertEqual(response.status_int, 302)
        self.assertEqual(response.location, 'http://localhost/login')


    def test_not_auth_redirect(self):
        response = self.testapp.get('/bills')
        self.assertEqual(response.status_int, 302)

    def test_email_already_exists(self):
        params = {'username': 'jasondearth',
                  'email': 'jasondearth@gmail.com',
                  'password': 'password',
                  'verify': 'password',
                  'name': 'Jason',
                  'lastname': 'Dearth'}
        self.testapp.post('/signup', params=params)
        duplicate = self.testapp.post('/signup', params=params)
        self.assertIn('Unable to create user for email', duplicate.body)


if __name__ == '__main__':
    unittest.main()
