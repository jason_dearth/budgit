__author__ = 'jasondearth'

import unittest
from models.security import User
from test_base import BaseTests


class TestUser(BaseTests):
    def test_insert_user(self):
        user = self.add_user()
        self.assertEqual(True, user[0])

    def test_duplicate_user(self):
        user = self.add_user()
        dup = self.add_user()
        self.assertEqual(True, user[0])
        self.assertEqual(False, dup[0])

    def test_password_change(self):
        user = self.add_user()
        hashed = user[1].password
        user[1].set_password('newPassword')
        self.assertNotEqual(hashed, user[1].password)

    def test_security_groups(self):
        user = self.add_user()
        self.assertEqual(['jdearth', 'user'], user[1].get_groups())
        user[1].add_group('admin')
        self.assertEqual(['jdearth', 'user', 'admin'], user[1].get_groups())
        user[1].remove_group('admin')
        self.assertEqual(['jdearth', 'user'], user[1].get_groups())

    def test_token_validation(self):
        user = self.add_user()
        user_id = user[1].get_id()
        token = User.create_signup_token(user_id)
        verified_user = User.get_by_auth_token(user_id, token, 'signup')
        self.assertEqual(user_id, verified_user[0].get_id())


if __name__ == '__main__':
    unittest.main()
