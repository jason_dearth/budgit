__author__ = 'jasondearth'

import datetime
import json
from google.appengine.ext import ndb
from test_base import BaseTests
from models.bills import Bill
from handlers.bills_handler import bill_key


class BillsJsonTests(BaseTests):
    @ndb.transactional
    def add_bill(self, user_key):
        testing_date = datetime.datetime.today()
        Bill.add_user_bill(description='my bill',
                           user_key=user_key,
                           next_due_date=testing_date,
                           last_due_date=testing_date,
                           amount=100, parent=bill_key())

    def call_api_add_bill(self, description='test bill',
                          next_due_date='2014-12-12',
                          last_due_date='2016-12-12',
                          amount=100, status='due',
                          credit=False, frequency='monthly',
                          frequency_factor=1):
        return self.testapp.post('/api/bills',
                                 json.dumps({'description': description,
                                             'nextDueDate': next_due_date,
                                             'lastDueDate': last_due_date,
                                             'amount': amount,
                                             'status': status,
                                             'credit': credit,
                                             'frequency': frequency,
                                             'frequencyFactor': frequency_factor}))

    def login(self):
        username = 'jdearth'
        password = 'jdearth'
        user = self.add_user(username=username,
                             password_raw=password,
                             verified=True)
        self.testapp.post('/login',
                          params={'username': username,
                                  'password': password})
        return user[1].key

    def test_bills_get_json(self):
        user = self.login()
        self.add_bill(user)
        self.add_bill(user)
        response = self.testapp.get('/api/bills')
        self.assertIn('description', response.body)
        self.assertEqual(response.status, '200 OK')

    def test_no_bills(self):
        self.login()
        response = self.testapp.get('/api/bills')
        self.assertIn('[]', response.body)
        self.assertEqual(response.status, '200 OK')

    def test_add_bill(self):
        self.login()
        response = self.call_api_add_bill()
        self.assertIn('"description": "test bill"', response.body)
        resp = self.testapp.get('/api/bills')
        self.assertIn('"description": "test bill"', resp.body)
        self.assertEqual(resp.status, '200 OK')

    def test_api_bill_delete(self):
        self.login()
        response = self.call_api_add_bill()
        js_response = json.loads(response.body)
        deleted = self.testapp.delete('/api/bill/' + str(js_response['id']))
        self.assertEqual(deleted.status, '200 OK')
        record_count = Bill.query()
        self.assertEqual(record_count.count(), 0)

    def test_api_bill_update(self):
        self.login()
        response = self.call_api_add_bill()
        js_response = json.loads(response.body)
        update = self.testapp.put('/api/bill/' + str(js_response['id']),
                                  json.dumps({'description': 'pretty sweet',
                                              'nextDueDate': '2020-02-02',
                                              'lastDueDate': '2020-02-22',
                                              'amount': 123.45,
                                              'status': 'due',
                                              'recurring': False,
                                              'credit': False,
                                              'frequency': 'daily',
                                              'frequencyFactor': 1}))
        self.assertEqual(update.status, '200 OK')
        updated = self.testapp.get('/api/bills')
        self.assertIn('pretty sweet', updated.body)