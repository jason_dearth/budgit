__author__ = 'jasondearth'

import unittest
import webtest
from google.appengine.ext import testbed
from main import app
from models.security import User


class BaseTests(unittest.TestCase):
    def setUp(self):
        self.testbed = testbed.Testbed()
        self.testbed.activate()
        self.testbed.init_datastore_v3_stub()
        self.testbed.init_memcache_stub()
        self.testbed.init_mail_stub()
        self.testapp = webtest.TestApp(app)

    def tearDown(self):
        self.testbed.deactivate()

    def add_user(self, username='jdearth', unique_properties=['email_address'],
                 email_address='jasondearth@gmail.com', name='Jason',
                 password_raw='password', last_name='Dearth', verified=False,
                 sec_groups=['jdearth', 'user']):
        return User.create_user(username, unique_properties,
                                email_address=email_address, name=name,
                                password_raw=password_raw, last_name=last_name,
                                verified=verified, sec_groups=sec_groups)


if __name__ == '__main__':
    unittest.main()
