__author__ = 'jasondearth'

import datetime
import unittest
from models.bills import Bill
from handlers.bills_handler import bill_key
from test_base import BaseTests


class MyTestCase(BaseTests):
    def add_bill(self, user_key, description='test bill',
                 next_due_date='2014-01-01', last_due_date='2014-01-01',
                 amount=0):
        if next_due_date:
            next_due_date = datetime.datetime.strptime(next_due_date,
                                                       '%Y-%m-%d')
        if last_due_date:
            last_due_date = datetime.datetime.strptime(last_due_date,
                                                       '%Y-%m-%d')
        return Bill.add_user_bill(description=description, user_key=user_key,
                                  next_due_date=next_due_date,
                                  last_due_date=last_due_date, amount=amount,
                                  parent=bill_key())

    def test_bill_creation(self):
        user = self.add_user()
        self.add_bill(user_key=user[1].key)
        bills = Bill.query()
        self.assertEqual(1, bills.count())

    def test_bill_deletion(self):
        user = self.add_user()
        key = self.add_bill(user_key=user[1].key)
        key.delete()
        bills = Bill.query()
        self.assertEqual(0, bills.count())

    def test_two_user_bills(self):
        user = self.add_user()
        self.add_bill(user_key=user[1].key)
        user2 = self.add_user(username='second',
                              email_address='second@user.com')
        self.add_bill(user_key=user2[1].key)
        self.add_bill(user_key=user2[1].key)
        bills = Bill.get_user_bills(user_key=user2[1].key, parent=bill_key())
        self.assertEqual(2, bills.count())


if __name__ == '__main__':
    unittest.main()
