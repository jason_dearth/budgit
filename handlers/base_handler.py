__author__ = 'jasondearth'

import os
import functools
import jinja2
import logging
import webapp2
from webapp2_extras import auth, sessions

template_dir = os.path.join(os.path.dirname(__file__), '../views')
jinja_env = jinja2.Environment(loader=jinja2.FileSystemLoader(template_dir),
                               extensions=['jinja2.ext.autoescape'],
                               autoescape=True)


def user_required(handler):
    """
        Decorator that checks if there's a user associated with a current
        session.
        Will also fail if there is no session present.
    """
    def check_login(self, *args, **kwargs):
        auth = self.auth
        if not auth.get_user_by_session():
            self.redirect(self.uri_for('login'), abort=True)
        else:
            return handler(self, *args, **kwargs)

    return check_login


def user_authorized(required):
    """
        Decorator that checks if the user is authorized.
        access = security groups listed for user
        required = security groups listed for function
        :param required:
        :return:
    """
    def dec(handler):
        @functools.wraps(handler)
        def wrapper(self, *args, **kwargs):
            u = self.user
            if len(set(u.get_groups())&set(required)) == 0:
                logging.error('SECURITY VIOLATION ATTEMPTED')
            else:
                return handler(self, *args, **kwargs)
        return wrapper
    return dec


class BaseHandler(webapp2.RequestHandler):
    @webapp2.cached_property
    def auth(self):
        """ Shortcut to access the auth instance as a property """
        return auth.get_auth()

    @webapp2.cached_property
    def user_info(self):
        """
            Shortcut to access a subset of the user attributes
            that are stored in the session.

            The list of the attributes to store in the session
            is specified in config['webapp2_extras.auth']['user_attributes']
            :return: A dictionary with most user information
        """
        return self.auth.get_user_by_session()

    @webapp2.cached_property
    def user(self):
        """
            Shortcut to access the currently logged in user.
            Unlike user_info, it fetches information from the
            persistence layer and return an instance of the
            underlying model.
            :return: The instance of the user model associated
             to the logged in user
        """
        u = self.user_info
        return self.user_model.get_by_id(u['user_id']) if u else None

    @webapp2.cached_property
    def user_model(self):
        """
            Returns the implementation of the user model.
            It is consistent with
                config['webapp2_extras.auth']['user_model'], if set.
        """
        return self.auth.store.user_model

    @webapp2.cached_property
    def session(self):
        """ Shortcut to access the current session """
        return self.session_store.get_session(backend='memcache')

    def render_template(self, view_filename, params=None):
        if not params:
            params = {}
        user = self.user_info
        params['user'] = user
        # logging.error(params)
        # logging.error(view_filename)
        t = jinja_env.get_template(view_filename)
        # logging.error(template_dir)
        self.response.out.write(t.render(params))

    def display_message(self, message):
        """ Utility function to display simple message """
        params = {
            'message': message
        }
        self.render_template('message.html', params)

    # this is needed for webapp2 sessions to work
    def dispatch(self):
        """ Gets a session store for this request """
        self.session_store = sessions.get_store(request=self.request)
        try:
            webapp2.RequestHandler.dispatch(self)
        finally:
            self.session_store.save_sessions(self.response)
