__author__ = 'jasondearth'

import datetime
import json
from google.appengine.ext import ndb
from base_handler import BaseHandler, user_required
from models.bills import Bill


def bill_key(name='default'):
    return ndb.Key('bills', name)


class BillHandler(BaseHandler):
    @user_required
    def delete(self, bill_id):
        key = ndb.Key('Bill', int(bill_id), parent=bill_key())
        key.delete()

    @user_required
    def put(self, bill_id):
        key = ndb.Key('Bill', int(bill_id), parent=bill_key())
        bill = key.get()

        json_request = json.loads(self.request.body)
        try:
            # get values from json
            description = json_request['description']
            next_due_date = json_request['nextDueDate']
            due_date = datetime.datetime.strptime(next_due_date[0:10], '%Y-%m-%d')
            last_due_date = json_request['lastDueDate']
            last_date = datetime.datetime.strptime(last_due_date[0:10], '%Y-%m-%d')
            amount = json_request['amount']
            status = json_request['status']
            credit = json_request.get('credit') or False
            frequency = json_request['frequency']
            frequency_factor = json_request['frequencyFactor']

            # update bill
            bill.description = description
            bill.next_due_date = due_date
            bill.last_due_date = last_date
            bill.amount = amount
            bill.status = status
            bill.credit = credit
            bill.frequency = frequency
            bill.frequency_factor = frequency_factor
            bill.put()
            self.response.out.write('successful update')
        except Exception as e:
            print e
            result = {
                'status': 'error',
                'status_code': '400',
                'error_message': 'Incoming data was not in correct format'
            }
            self.response.out.write(json.dumps(result))


class BillsHandler(BaseHandler):
    def render_json(self, bill):
        json_text = json.dumps(bill)
        self.write(json_text)

    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    @user_required
    def get(self):
        user = self.user.key
        bills = Bill.get_user_bills(user_key=user,
                                    parent=bill_key())
        if bills.count() > 0:
            return self.render_json([bill.as_dict() for bill in bills])
        return self.render_json([])

    @user_required
    def post(self):
        user = self.user.key
        json_request = json.loads(self.request.body)
        try:
            # parse incoming
            description = json_request['description']
            next_due_date = json_request['nextDueDate']
            due_date = datetime.datetime.strptime(next_due_date[0:10], '%Y-%m-%d')
            last_due_date = json_request.get('lastDueDate') or False
            if last_due_date:
                last_date = datetime.datetime.strptime(last_due_date[0:10], '%Y-%m-%d')
            else:
                last_date = False
            amount = json_request['amount']
            status = json_request.get('status') or 'due'
            credit = json_request.get('credit') or False
            frequency = json_request['frequency']
            frequency_factor = json_request.get('frequencyFactor') or 1

            # create bill
            bill = Bill.add_user_bill(description=description,
                                      user_key=user, next_due_date=due_date,
                                      last_due_date=last_date, amount=amount,
                                      status=status, credit=credit,
                                      parent=bill_key(), frequency=frequency,
                                      frequency_factor=frequency_factor)
            response = bill.get()
            return self.render_json(response.as_dict())
        except Exception as e:
            print e
            result = {
                'status': 'error',
                'status_code': '400',
                'error_message': 'Incoming data was not in correct format'
            }
            self.response.out.write(json.dumps(result))
